package update.pullrequest;

import akka.actor.ActorRef;
import akka.event.EventStream;
import models.AbstractPersistenceIntegrationTest;
import models.ContributionEvent;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.BeforeClass;
import org.junit.ClassRule;

import update.ActorRule;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * AbstractPullRequestImporterTest
 */
public class AbstractPullRequestImporterTest extends AbstractPersistenceIntegrationTest {

    @ClassRule
    public static ActorRule actorRule = new ActorRule();

    private static ActorRef pullRequestImporterActor;
    protected static EventStream eventStream;

    protected static final String repositoryId = "better-test-repo";
    protected static JsonNode firstResponse;
    protected static JsonNode secondResponse;

    @BeforeClass
    public static void loadResponses() {
        // setup PullRequestImporter actor
        eventStream = actorRule.eventStream();
        pullRequestImporterActor = actorRule.actorOf(PullRequestImporter.class);
        eventStream.subscribe(pullRequestImporterActor, ContributionEvent.class);

        firstResponse = loadJson("conf/pullrequests_api_sample");
        secondResponse = loadJson("conf/pullrequests_api_sample_updated");
    }

    private static JsonNode loadJson(String location) {
        File file = new File(location);
        assertTrue(file.exists());
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper();
        try (JsonParser parser = factory.createParser(file)) {
            return mapper.readTree(parser);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
