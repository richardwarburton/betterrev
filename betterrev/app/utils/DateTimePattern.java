package utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * DateTimeUtil class exposes methods to format Joda DateTime to different time formats.
 *
 */
public enum DateTimePattern {
    
    DEFAULT {
        @Override
        protected String getFormatString() {
            return "dd MMMM yyyy kk:mm:ss";
        }
    };
    
    protected abstract String getFormatString();
    
    private final DateTimeFormatter dateTimeFormatter;
    
    private DateTimePattern() {
        dateTimeFormatter = DateTimeFormat.forPattern(getFormatString());
    }

    /**
     * Method accepts DateTime value of type org.joda.time.DateTime and produce formatted output from Pattern.DEFAULT
     *
     * @param dateTime of type org.joda.time.DateTime
     * @return String
     * @throws NullPointerException
     */
    public static String format(DateTime dateTime) throws NullPointerException {        
        return DEFAULT.formatDateTime(dateTime);
    }

    /**
     * Method accepts DateTime value of type org.joda.time.DateTime and produce formatted output from given Pattern
     * @see utils.Pattern
     *
     * @param dateTime of type org.joda.time.DateTime
     * @param pattern of type util.Pattern
     * @return String
     * @throws NullPointerException
     */
    public String formatDateTime(DateTime dateTime) throws NullPointerException {
        if(dateTime == null){
            throw new NullPointerException("Invalid parameter.DateTime could not be null");
        }
        
        return dateTimeFormatter.print(dateTime);
    }
}
