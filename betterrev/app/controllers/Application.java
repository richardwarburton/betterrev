package controllers;

import models.Contribution;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;
import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Welcome to Betterrev"));
    }

    public static Result contributions() {
        List<Contribution> contributionList = Contribution.find.all();
        return ok(contributions.render(contributionList));
    }

    public static Result contribution(Long id) {
        Contribution contributionList = Contribution.find.byId(id);
        return ok(contribution.render(contributionList));
    }

    public static Result contactUs() {
        return ok(contactus.render());
    }

    public static Result help() {
        return ok(help.render());
    }

}
